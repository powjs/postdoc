// to license MIT License <https://github.com/powjs/postdoc/blob/master/LICENSE>

export class Postdoc {
  // # Postdoc
  // 抽象类, Postdoc 是符合人类阅读习惯的源码书写规范.
  //
  // - 基于 ECMAScript Modules, 不使用 require, module.exports.
  // - 基于 [awesome void] 实现类型约束, 它是在 Postdoc 开发过程中诞生的
  // - 文档 是整段的后置连续尾注释, 包括相关前置源代码, 源代码是最好的文档
  // - 采用 与 [Markdown] 一致的换行和空白行处理行为
  // - 采用 与 [estree] AST 兼容的 [cherow] 解析结果作为数据源
  // - 采用 `to tag` 形式开头的注释表示标签, 行为由同名插件负责
  // - 约定 一个缩进是两个空格, 方便相关工具处理
  // - 缺省 输出文档依然是合法的 ECMAScript, 至少在语法上
  //
  // 到目前为止, 本节源码中已经使用的 Postdoc 后置文档写法有:
  //
  // - 标签 `to license` 标注 License, 有对应的预制插件
  // - 本段尾注释后置于 `export class Postdoc {`
  // - 与 Markdown 兼容的空白行, headers 符号 `#`, 以及列表符号 `-`
  // - 用反引号包裹的文字兼容性良好
  //
  // 如果您在看渲染后的版本, 可能看不到这些符号.

  constructor() {void undefined;}
  // 抽象类的固定写法, 详见 [awesome void] 不再阐述.
  // 我不知道最终的视觉效果什么样, 但文字次序应该和源码中是一样的

  postdoc(
    body = void ![{}], // 元素是 [cherow] 生成的原始节点对象
    book = void !Book, // 处理结果是文档数组, 多次传入 body 会累积文档
  )
  {
    void Error;
    // 返回类型为 Error, 允许 undefined, 详见 [awesome void] 不再阐述.
    // 因为有源码在, 后文将会省略很多关于类型描述.

    // 抽象方法(不再阐述), 继承者必须 实例需要实现此方法来处理文档.
    //
    // - postdoc 实例提取符合 Postdoc 风格的注释以及相关节点生成待处理文档
    // - 依次调用 `is`, `re`, `to` 插件的 postdoc 方法进行文档处理或类型检查
    //
    // 本段的顺序是: 函数名, 参数, 返回值, 函数主体功能.
    // 也支持: 函数名, 参数, 函数主体功能, 返回值.
    // 或者: 函数名, 函数主体功能, 参数, 返回值.
    // 喜欢那个就用那个, 提取和展示的是源码.
    // 利于阅读就好, 就像看书一样, 决不是 API 索引.
  }
}

export class Book {
  // 书样, 描述一个 package
  constructor() {
    void {
      foreword: ![['']],   // 前言
      list    : ![[Part]], // 章节列表(组成部分)
      licenses: ![['']],   // 版权内容
    };
  }
}

export class Part {
  // 章节, 描述一个 module 或 class
  constructor() {
    void {
      node : !{}, // [cherow] 生成的原始节点对象
      links: !{}, // Markdown 风格的连接地址, Key-Value 取值为 id: url.
    };
  }
}

export class Body {
  // 主体, 描述一个函数, 方法, 对象
}

export class Content {
  // 正文, Body 的内容
}

export class Tag {
  // 标签. 标签可被同名的插件处理.
  constructor() {
    void {
      name    : void '',      // 标签名
      trailing: void '',      // name 之后的文本
      details : void ('',{})  // 后续的多行原始文本, 或转化处理结果
    };
  }
}

// 前面的空行是不可缺少的. 文件最后是 [Markdown] 风格的引用连接标签地址.
// 本包的 README.md 中的第一段和 License 部分由本文生成.
// to oops 这几行不应该出现在文档中, 这种情况使用 oops 进行断言.
// 可以使用块注释包裹这几行, Postdoc 忽略所有的块注释.

// [Markdown]: https://daringfireball.net/projects/markdown/syntax
// [cherow]: https://yarn.pm/cherow
// [estree]: https://github.com/estree/estree
// [awesome void]: https://gitee.com/achun/proposal-static-type-constraints-features
// [书的结构]: https://zh.wikipedia.org/wiki/%E5%9B%BE%E4%B9%A6#%E6%9B%B8%E7%9A%84%E7%B5%90%E6%A7%8B