import {Postdoc} from './types';
export default class exist extends Postdoc {
  // 安全措施, 表示不应该被最初的文档过滤掉, 否则抛出错误.
  // 行为正好和 oops 相反. 相同逻辑的地方使用一次就行, 不必每个地方都用.
}
