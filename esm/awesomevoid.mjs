class Defender {
  // 抽象类, 类型捍卫者, 用于编译期或运行期需要特别处理的自定义类型.
  // 注意 ECMAScript 和 [static-type] 中都没有接口的概念.
  // [static-type] 是严格类型描述, 抽象类必须被显示的继承且重载所有方法.

  constructor() {void undefined;}
  void (
    path     = void !'',     // 类型路径
    defender = void Defender // 注册时使用
  ) {
    void !(false, Defender);
  }

  form(x = void !undefined) {
    // 抽象方法, 转换 default x 到类型实例
    // 实现必须重新定义该方法的参数和返回类型
    void !undefined;
  }
}

class Awesome extends Defender {
  // to exist awesome 单例对象原型, 捍卫全局的 defender.
  // 本模块导出了该单例.
  void (path, defender) {
    void !(false, Defender);
  }

  form(key = void !'') {
    void undefined;
  }
}

const awesome = new Awesome;

export {awesome, Defender};