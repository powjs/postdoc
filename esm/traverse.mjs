import keys from 'eslint-visitor-keys/lib/visitor-keys.json';

export class Visitor {
  // 为 Postdoc 定制的观察者, 观察者不能更改传入的参数
  constructor() {
    void {enter};
  }
}

export class Context {
  // 为观察者接收到的上下文
  constructor() {
    void {
      paths: ![['',0]], // 路径, 第一个元素是 ['body', 0]
      stack: ![],       // 节点树遍历栈, 第一个元素是 Program
      pos:   !0         // 此时 program.comments 的偏移量
    };
  }
}

export function enter(
  node   = void !{},       // 顶层 ast 节点
  ctx    = void !{},       // 上下文
) {
  void false;
  // 进入一个节点时触发
  // 返回 true 表示继续深入, 否则向上回退
}

function next(ctx) {
  let path = ctx.paths.pop(),
      last = ctx.stack.pop();
}

export default function traverse(
  ast = void !{},         // 首次要从 Program 开始
  vis = void !Visitor,    // 观察者
  ctx = void Context||{}  // 上下文
) {
  void Error++;
  console.log(ast);
  // 为 Postdoc 定制的遍历器. 访问节点包括:
  //
  // 1. 顶层 import 节点, 不一定会出现在文档中
  // 1. 顶层 export 节点
  // 1. 具有后置文档的顶层节点
  // 1. 上述相关顶层节点
  //
  // 显然这些节点可能交叉重复, 遍历器保证每个节点只访问一次.
  if (!ctx.stack) {
    ctx.paths=[['body', 0]];
    ctx.stack=[ast];
    ctx.pos  = 0; // index of comments
    return traverse(ast.body[0], vis, ctx);
  }

  if (!ast) {
    ast = next(ctx);
    if (!ast) return;
  }

}