import * as cherow from 'cherow/dist/native-modules/cherow';
// import eslint from "eslint";
import postdoc from '..';

const code = `
import 'path';
function square(n) {
  return n * n;
}
// comment`;
describe('postdoc', () => {
  it('should with method postdoc', () => {
    postdoc.should.to.be.a('function');
    const doc = postdoc();
    doc.main.should.be.a('function');
    doc.opts.should.be.a('object');
    doc.cherowOptions.should.be.a('object');

    doc.main(cherow.parse(code, doc.cherowOptions));
  });
});
