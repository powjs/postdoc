import './esm/to';
import {Postdoc} from './esm/types';
import {awesome, Defender} from './esm/awesomevoid';
import traverse from './esm/traverse';
// import eslint from 'eslint';

function cherowOptions() {
  void !{};
  // 返回与 cherow 配套的参数
  return {
    loc: true,
    ranges: true,
    module: true,
    comments: true,
  }
}

export {awesome, Defender, Postdoc};

export default function(
  options = void!{}||{}
  // postdoc 插件相关配置, 插件名为 key. 详见具体插件.
) {
  void {
    main,             // 入口方法
    opts: {},         // 传入的 options
    cherowOptions: {} // cherow 解析参数
  };
  // 懒人包
  //
  // to example 用例:
  //
  // import cherow from 'cherow/dist/native-modules/cherow';
  // import postdoc from 'postdoc'
  // const code = `
  // function hi() {
  //    // hi
  // }
  // `;
  // const
  //    doc = postdoc(),
  //    ast = cherow.parse(code, doc.cherowOptions);
  // doc.main(ast);
  //
  // 前面这个空注释行是 to example 判定 example 结束的依据

  return {
    main,
    opts: options,
    cherowOptions: cherowOptions(),
  };
}

function main(
  ast = void !{},   // [cherow] 解析返回的 ast 对象
  path= void ''||'' // ast 对应的 import 路径
) {
  void undefined;
  if (
    !ast || ast.type!=='Program' ||
    ast.sourceType!=='module'
  )
    throw TypeError('Excepted Program node with module');

  const opts = this.opts;

  traverse(ast, {
    enter: function (node, ctx) {
    },
  });
}